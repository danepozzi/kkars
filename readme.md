STACK INTERCHANGE
======


Repository for the game-inspired audiovisual performance developed by Daniele Pozzi and Kosmas Giannoutakis for the "[GAPPP](http://gappp.net) - Gamified Audiovisual Performance and Performance Practice" final symposium.

March 29th, 2019, Mumuth.
University of Music and Performing Arts Graz, Austria.

______

Program notes:

The video game “Freeway” designed by David Crane for the Atari 2600 and published by Activision in 1981, served as the starting point for the development of this audiovisual game performance. The original game rules are inverted so that the goal is to collide with the moving objects. Each collision transforms visually the freeway, creating complex self-similar patterns. The game mechanics are coupled to two computer music systems which generate unpredictable sonic patterns by means of generative feedback networks. The performers play the visual game and at the same time improvise music, by controlling some parameters of their computer music systems.


![](beam.png)
