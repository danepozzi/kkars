void setup() {
  frameRate(50);
  //fullScreen(2);
  size(displayWidth, displayHeight);
  println(OS); 
  standardSetup();
  
  if(gameState==0){
    setupState0();
  }else if(gameState==1){
    setupState1();
  }else if(gameState==2){
    setupState2();
  }else if(gameState==3){
    setupState3();
  };
 
 
  oscSetup();
}

void draw() {
  
  if (gameState == 0) {
    state0();
  }else if (gameState == 1){
    state1();
  }else if(gameState == 2){
    state2();
  }else if(gameState == 3){
    if (updateOnce){
      trackFrame = frameCount;
      updateOnce = false;
    };
    state3();
  };
  
  //println("anewgTrans: "+anewgTrans);
  //println("rRms: " + rRms);
  //println(frameRate);
  time();
}
