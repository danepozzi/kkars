class Pedestrian { 
  color c;
  float xpos;
  float ypos;
  float yspeed;
  int direction = 1;
  int collided = 1;
  int pbLife = 20;

  // The Constructor is defined with arguments.
  Pedestrian(color tempC, float tempXpos, float tempYpos, float tempYspeed) { 
    c = tempC;
    xpos = tempXpos;
    ypos = tempYpos;
    yspeed = tempYspeed;
  }

  void display() {
    
    stroke(0);
    fill(c);
    rectMode(CENTER);
    rect(xpos,ypos,3,3);
    
  }

  void walk() {
    if (direction == 0) {
      ypos = ypos + yspeed;
      if (ypos > height) 
        ypos = 0;
      }
    if (direction == 1) {
    ypos = ypos - yspeed;
      if (ypos < 0) 
        ypos = height;
    }
  }
  
  void update(float newSpeed) {
    yspeed = newSpeed;
  }
  
  void updateDir(int newDir) {
    direction = newDir;
  }
  
  void updateColor() {
    c = color(random(255), random(255), random(255));
  }
  
  void updateCollided(int coll) {
    collided = coll;
  }
  
  void pushBack() {
    if(pbLife >0) {
      direction = 0;
      pbLife = pbLife -1;
    }else{
      //println(pbLife);
      direction = 1;
      pbLife = 20;
    }
  }
  
  float getY(){
    return ypos;
  }
  
  float getX(){
    return xpos;
  }
  
  int getCollided(){
    //println(xpos);
    return collided;
  }
  
  int getpbLife() {
    return pbLife;
  }
}
