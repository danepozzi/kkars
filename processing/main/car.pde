class Car { 
  color c;
  float xpos;
  float ypos;
  float xspeed;
  float xxspeed;
  int direction = round(random(100)/100);
  int base = 10;
  int[] tracex = new int[width];
  int bufferIndex = 0;
  //int[] tracey = new int[0];

  // The Constructor is defined 2with arguments.
  Car(color tempC, float tempXpos, float tempYpos, float tempXspeed) { 
    c = tempC;
    xpos = tempXpos;
    ypos = tempYpos;
    xspeed = tempXspeed;
  }

  void display() {
    stroke(0);
    //noStroke();
    fill(c);
    rectMode(CENTER);
    rect(xpos,ypos,8,base,10);
  }
  
  void displayNoStroke() {
    //stroke(0);
    noStroke();
    fill(c);
    rectMode(CENTER);
    rect(xpos,ypos,8,base,10);
  }
  
  void displayBlack() {
    stroke(0);
    fill(color(0));
    rectMode(CENTER);
    rect(xpos-(xpos/2),ypos,8,4);    
  }

  void drive() {
    if (direction == 0) {
      xpos = xpos + xxspeed;
      if (xpos > width) 
        xpos = 0;
      }
    if (direction == 1) {
    xpos = xpos - xxspeed;
      if (xpos < 0) 
        xpos = width;
    }
      
    
  }
  
  void update(float newSpeed) {
    xxspeed = xspeed*newSpeed+xspeed;
  }
  
  void updateBase(int newBase) {
    base = newBase;
  }
  
  float getX(){
    //println(xpos);
    return xpos;
  }
  
  float getY(){
    //println(xpos);
    return ypos;
  }
  
  void updateColor(color newColor) {
    c = newColor;
  }
  
  void recordTrace() {
    tracex[bufferIndex] = int(xpos);
    bufferIndex = bufferIndex + 1 % width;
  }
  
  void displayTrace() {
  for (int i = 0; i < width; i++) {
      rect(tracex[i], ypos, 4, 4);
   }
}
}
