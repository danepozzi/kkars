void time(){
  int ms = ceil(millis()/1000);
  int s = ms%60;
  int m = ceil(s/60);
  textSize(25);
  fill(255);
  text(m + " : " + s, width-50,10 );
}
