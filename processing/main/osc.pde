import oscP5.*;
import netP5.*;
OscP5 oscP5;
NetAddress puredata, superCollider;

void oscSetup() {
  oscP5 = new OscP5(this,12000);
  if (OS.contains("Mac")) {
    println("Host: Daniele");
    puredata = new NetAddress("192.168.178.192",8000);
    superCollider = new NetAddress("127.0.0.1",57120);
    oscP5.send(new OscMessage("/host").add("daniele"), superCollider);
  }else{
    println("Host: Kosmas");
    puredata = new NetAddress("127.0.0.1",8000);
    superCollider = new NetAddress("192.168.178.103",57120);
    oscP5.send(new OscMessage("/host").add("kosmas"), superCollider);
  };
  oscP5.send(new OscMessage("/pd_connect").add(0), puredata);
}

void oscEvent(OscMessage theOscMessage) {
  if(theOscMessage.checkAddrPattern("/rms_oscill") == true) {
    //println(theOscMessage);
    if(theOscMessage.checkTypetag("f")) {
      float rms = theOscMessage.get(0).floatValue();
      //println("rms: "+rms);
      interpTime = 0.01;
      anewgTrans = (int(rms*1500));
      return;
    }
  };
  if(theOscMessage.checkAddrPattern("/rms_right") == true) {
    if(theOscMessage.checkTypetag("f")) {
      float OSCvalue = theOscMessage.get(0).floatValue();
      //println(" values: "+OSCvalue);
      rRms = OSCvalue*1000;
      return;
    }
  };
}
