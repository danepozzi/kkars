void detectCollision(){
  for (int i = 0; i < cars.length; i++) {
    for (int j = 0; j < pedestrians.length; j++) {
      float distance = dist(cars[i].getX(), cars[i].getY(), pedestrians[j].getX(), pedestrians[j].getY());
      if (distance < 20) {
        if(pedestrians[j].getCollided() == 0) {
          //pedestrians[j].updateColor();
          pedestrians[j].updateCollided(1);
          pedestrians[j].pushBack();
      }else{
        if(pedestrians[j].getpbLife() > 0 && pedestrians[j].getpbLife() != 20){
          pedestrians[j].pushBack();
        };
        if(pedestrians[j].getCollided() == 1) {
          pedestrians[j].updateCollided(0);
        };
      };
    };
  };
  };
}

//DANIELE
void detectCollisionRadatar1(){
  for (int i = 0; i < cars.length; i++) {
      float distance = dist(cars[i].getX(), cars[i].getY(), radatar1.getX(), radatar1.getY());
      if (distance < 10) {
        if(getCollidedArray[i] == 0) {
          float speed = random(2);
          pedestrians[pedGenIndex] = new Pedestrian(color(125, random(120,255), 255), pedGenIndex*5+400, height, speed);
          pedGenIndex ++;
          getCollidedArray[i] = 1;
          
          //pedestrians[j].updateColor();
          //radatar1.updateCollided(i,1);
          println(pedGenIndex);
          randomizePerspective();
          //interpTime = random(0.08)+0.01;
          newgTransX = perspectives[gameState][perspectiveNumber][0];
          newgTransY = perspectives[gameState][perspectiveNumber][1];
          newgScale = perspectives[gameState][perspectiveNumber][2];
          newgRotation = perspectives[gameState][perspectiveNumber][3];
          //cars[i].updateColor(color(random(125,255), 55, 255));
          cars[i].updateColor(color(random(10,255)));
          cars[i].updateBase(4);
          oscP5.send(new OscMessage("/collision").add(pedGenIndex).add(interpTime), superCollider);
          println(pedGenIndex);
      }
    }
    /*else{
        if(radatar1.getCollided(i) == 1) {
          radatar1.updateCollided(i,0);
        };
      };*/
  };
}

//KOSMAS
void detectCollisionRadatar2(){
  for (int i = 0; i < cars.length; i++) {
      float distance = dist(cars[i].getX(), cars[i].getY(), radatar2.getX(), radatar2.getY());
      if (distance < 10) {
        if(getCollidedArray[i] == 0) {
          float speed = random(2);
          pedestrians[pedGenIndex] = new Pedestrian(color(255, random(120,255), 255), pedGenIndex*5+400, height, speed);
          pedGenIndex ++;
          getCollidedArray[i] = 1;
          //pedestrians[j].updateColor();
          //radatar2.updateCollided(i,1);
          println(pedGenIndex);
          //println(collided);
          randomizePerspective();
          //interpTime = random(0.08)+0.01;
          newgTransX = perspectives[gameState][perspectiveNumber][0];
          newgTransY = perspectives[gameState][perspectiveNumber][1];
          newgScale = perspectives[gameState][perspectiveNumber][2];
          newgRotation = perspectives[gameState][perspectiveNumber][3];
           //cars[i].updateColor(color(random(125,255), 55, 255));
          cars[i].updateColor(color(random(10,255)));
          cars[i].updateBase(4);
          oscP5.send(new OscMessage("/collision").add(pedGenIndex).add(interpTime), puredata);
          println(pedGenIndex);
      }
      }
      /*else{
        if(radatar2.getCollided(i) == 1) {
          //radatar2.updateCollided(i,0);
        };
      };*/
  };
}


void detectCollisionCenter(){
  float centerDistRad1 = dist(radatar1.getX(), radatar1.getY(), 525, 525);
  float centerDistRad2 = dist(radatar2.getX(), radatar2.getY(), 525, 525);
    if (centerDistRad1 < 40.0 && centerDistRad2 < 40.0){
      println("center");
      randomizePerspective();
      //interpTime = random(0.08)+0.01;
      newgTransX = perspectives[gameState][perspectiveNumber][0];
      newgTransY = perspectives[gameState][perspectiveNumber][1];
      newgScale = perspectives[gameState][perspectiveNumber][2];
      newgRotation = perspectives[gameState][perspectiveNumber][3];
      radatar1.setCoordinates(int(random(width)), int(random(height)));
      radatar2.setCoordinates(int(random(width)), int(random(height)));
      avatarCollNum ++;
      oscP5.send(new OscMessage("/avatarCollNum").add(avatarCollNum), puredata);
      oscP5.send(new OscMessage("/avatarCollNum").add(avatarCollNum), superCollider);
    }
} 
