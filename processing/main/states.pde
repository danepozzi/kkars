void standardSetup(){
  radatar1 = new Radatar(color(0, 100, 100), 600, 150, 1, 0, numCar);
  radatar2 = new Radatar(color(100, 100, 0), 300, 150, 1, 1, numCar);
  //xtrace = new int[0];
  //ytrace = new int[0];
  getCollidedArray = new int[numCar];
  cars = new Car[numCar];
  for (int i = 0; i < cars.length; i++) {
    cars[i] = new Car(color(random(10,255), 50), random(300), i*5+400, random(2)); 
  };
};

void setupState0(){
  pedestrians = new Pedestrian[numPed];
  for (int i = 0; i < pedestrians.length; i++) {
    float speed = random(2);
    pedestrians[i] = new Pedestrian(color(random(0)), -400, height, speed);
    background(255);
  };
};

void setupState1(){
  pedestrians = new Pedestrian[numPed];
  for (int i = 0; i < pedestrians.length; i++) {
    float speed = random(2);
    pedestrians[i] = new Pedestrian(color(random(0)), -400, height, speed);
    background(0);
  };
};

void setupState2(){
  pedestrians = new Pedestrian[numPed];
  for (int i = 0; i < pedestrians.length; i++) {
    float speed = random(2);
    pedestrians[i] = new Pedestrian(color(255, random(120,255), 255), i*5+400, height, speed);
    background(0);
  };
}

void setupState3(){
  pedestrians = new Pedestrian[numPed];
  for (int i = 0; i < pedestrians.length; i++) {
    float speed = random(2);
    pedestrians[i] = new Pedestrian(color(255, random(120,255), 255), i*5+400, height, speed);
    background(0);
  };
}

void state0(){
  int curTimeMs = millis();
  int startupTimeRemainingMs = startDelayMs - (curTimeMs - startTimeMs);
  textSize(50);
  fill(0);
  textAlign(CENTER,CENTER);
  atStartup = startupTimeRemainingMs > 0;
  if(millis()>5000){
    background( 255 - min(((millis()-5000)/18),255));
  }else{
    background(255);
    for (int i = 0; i < cars.length; i++) {
      //cars[i].drive();
      cars[i].displayNoStroke();
      //cars[i].displayBlack();  //uncomment this line to slowly remove their past
      //cars[i].update(lRms);
      //cars[i].update(mouseY, 0);
    };
  };
  fill( #000000 );
  text( "STACK INT  RCHANGE",width/2,height/2-100 );
  text(ceil(startupTimeRemainingMs/1000.0), width/2+12, height/2-100);
  
  textSize(25);
  if(millis() > 5000) {  
    text( "HIT AS MANY CARS AS POSSIBLE",width/2,height/2 );
    for (int i = 0; i < cars.length; i++) {
      cars[i].drive();
      cars[i].displayNoStroke();
      //cars[i].displayBlack();  //uncomment this line to slowly remove their past
      cars[i].update(lRms);
      //cars[i].update(mouseY, 0);
    };
  };
  if(millis() > 10000) {  
      gameState = 1;
      oscP5.send(new OscMessage("/gameState").add(gameState), puredata);
      oscP5.send(new OscMessage("/gameState").add(gameState), superCollider);
      println("gameState: " + gameState);
  };
};


void state1(){
  gTransX = lerp(gTransX, newgTransX, interpTime);
  gTransY = lerp(gTransY, newgTransY+anewgTrans, interpTime);
  gScale = lerp(gScale, newgScale, interpTime);
  gRotation = lerp(gRotation, newgRotation, interpTime);
  translate(gTransX, gTransY);
  scale(gScale);
  rotate(radians(gRotation));
  pushMatrix();
  //translate(mouseX, mouseY);
  //rotate(radians(rotation));
  //recordAvatarTrace();
  
  if(frameCount % (pedGenIndex+1) == 0) {  
    //background(frameCount%2 *120);
    background(0);
    //displayTrace(xtrace, ytrace);
  };

  //scale(mouseY/900*5+0.5);
  for (int i = 0; i < cars.length; i++) {
    cars[i].drive();
    cars[i].display();
    //cars[i].displayBlack();  //uncomment this line to slowly remove their past
    cars[i].update(rRms);
    //cars[i].update(mouseY, 0);
  };
  for (int i = 0; i < pedestrians.length; i++) {
    pedestrians[i].walk();
    pedestrians[i].display();
      //cars[i].update(mouseX, 0);
  };
  radatar1.display();
  radatar2.display();
  detectCollision();
  if(pedGenIndex<numCar){
    detectCollisionRadatar1();
    detectCollisionRadatar2();
  }else{
    if(oneShotState2){
      timeState2 = millis();
      oneShotState2 = false;
    };
    if(millis()-timeState2 < 5000){
      fill(255);
      text( "NOW REACH THE CENTER OF THE INTERSECTION",width/2,height/2-100);
    }else{
      background(0);
      gameState = 2;
      oscP5.send(new OscMessage("/gameState").add(gameState), puredata);
      oscP5.send(new OscMessage("/gameState").add(gameState), superCollider);
      println("gameState: " + gameState);
    };
  };
  //avatar.walk();
  //avatar.display();
  //noFill();
  //stroke(255);
  //rect(525,525,20,20);
  popMatrix(); 
  detectCollisionCenter();
};

void state2(){
  oscP5.send(new OscMessage("/gameState").add(gameState), puredata);
  gTransX = lerp(gTransX, newgTransX, interpTime);
  gTransY = lerp(gTransY, newgTransY+anewgTrans, interpTime);
  gScale = lerp(gScale, newgScale, interpTime);
  gRotation = lerp(gRotation, newgRotation, interpTime);
  translate(gTransX, gTransY);
  scale(gScale);
  rotate(radians(gRotation));
  pushMatrix();
  
  for (int i = 0; i < cars.length; i++) {
    cars[i].drive();
    cars[i].display();
    //cars[i].displayBlack();  //uncomment this line to slowly remove their past
    cars[i].update(rRms);
    //cars[i].recordTrace();
  };
  for (int i = 0; i < pedestrians.length; i++) {
    pedestrians[i].walk();
    pedestrians[i].display();
  };
  radatar1.display();
  radatar2.display();
  //rect(525,525,40,40);
  popMatrix(); 
  detectCollisionCenter();
  if (avatarCollNum == 5){
    gameState = 3;
    oscP5.send(new OscMessage("/gameState").add(gameState), puredata);
    oscP5.send(new OscMessage("/gameState").add(gameState), superCollider);
    println("gameState: " + gameState);
  }
}

void state3(){
  oscP5.send(new OscMessage("/gameState").add(gameState), puredata);
  gTransX = lerp(gTransX, newgTransX, interpTime);
  gTransY = lerp(gTransY, newgTransY+anewgTrans, interpTime);
  gScale = lerp(gScale, newgScale, interpTime);
  gRotation = lerp(gRotation, newgRotation, interpTime);
  translate(gTransX, gTransY);
  scale(gScale);
  rotate(radians(gRotation));
  pushMatrix();
  
  if (updateFrame){
    background(255);
    if (frameCount > (trackFrame + 200)) {
      updateFrame = false;
    };
  };
  
  
  /*if(frameCount % 300 == 0) {  
    //background(frameCount%2 *120);
    background(0);
    //displayTrace(xtrace, ytrace);
  };
  */

  
  for (int i = 0; i < cars.length; i++) {
    //cars[i].displayTrace();
    cars[i].drive();
    cars[i].display();
    //cars[i].displayBlack();  //uncomment this line to slowly remove their past
    cars[i].update(rRms);
    //cars[i].recordTrace();
  };
  for (int i = 0; i < pedestrians.length; i++) {
    pedestrians[i].walk();
    pedestrians[i].display();
  };
  radatar1.display();
  radatar2.display();
  //rect(525,525,20,20);
  popMatrix(); 
  detectCollisionCenter();
}
