class Radatar {
  color c;
  float ang = 0;
  float speed = 3;
  float x, y;
  int collided = 0;
  int pbLife = 20;
  int id = 0;
  
  
  Radatar(color tempC, float tempXpos, float tempYpos, float tempSpeed, int idn, int getCollidedN) { 
    c = tempC;
    x = tempXpos;
    y = tempYpos;
    speed = tempSpeed;
    id = idn;
  }
  
  void display() {
    pushMatrix();
    translate(x, y);
    rotate(ang);
    stroke(0);
    fill(c);
    rect(-20, -7.5, 7, 7); // Center the rect
    //translate(20, 0); // Translate to default Polar Coordinate 0 radians
    //ellipse(0, 0, 5, 5); // Display direction
    popMatrix();
  
    x += cos(ang)*speed;
    y += sin(ang)*speed;

    if(id == 0) {
      if(keys[0]) {
          x += cos(ang)*speed;
          y += sin(ang)*speed;
      };
      if(keys[1]) {
          x -= cos(ang)*speed;
          y -= sin(ang)*speed;
      };
      if(keys[2]) {
          ang -= 0.05;
         //println(left1);
      };
      if(keys[3]) {
          ang += 0.05;
         // println("right1");
      };
      };
      if(id == 1) {
        if(keys[4]) {
            x += cos(ang)*speed;
            y += sin(ang)*speed;
        };
        if(keys[5]) {
            x -= cos(ang)*speed;
            y -= sin(ang)*speed;
        };
        if(keys[6]) {
            ang -= 0.05;
        };
        if(keys[7]) {
            ang += 0.05;
        };
        };
  checkEdges();
  }

  float getX(){
    //println(xpos);
    return x;
  }
  
  float getY(){
    //println(xpos);
    return y;
  }
  
  int getCollided(int index){
    //println(xpos);
    return getCollidedArray[index];
  }
  
  void setCoordinates(int newX, int newY){
    x = lerp(x, newX, 1);
    y = lerp(y, newY, 1);
    
  }
  
  void checkEdges() {
    
    if (x > width) {
      x = 0;
    } else if (x < 0) {
      x = width;
    }
    
    if (y > height) {
      y = 0;
    } else if (y < 0) {
      y = height;
    }

  }
   
}
